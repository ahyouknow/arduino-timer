#include <IRremote.h>
#include <LiquidCrystal.h>
// timer and display
LiquidCrystal lcd(7, 8, 9, 10, 11, 12);
bool timerOn = false;
unsigned long timer = 0; // seconds
const unsigned int secondInterval = 1000;
unsigned long previousMillis = 0;

// IR blaster
unsigned long lastSignal = 0;
int cursor = 0;

void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  setupTimer();
  lcd.blink();  

  //IR BLASTER setup
  IrReceiver.begin(2);

  // Active Buzzer
  pinMode(3,OUTPUT);
}

void loop() {
  unsigned long currentMillis = millis();
  if (timerOn && (currentMillis - previousMillis >= secondInterval)){
    lcd.clear();
    lcd.print("Timer on");
    timer -= 1;
    timerFormat();
    previousMillis = currentMillis;
  }
  else if (timerOn && timer <= 0){
    lcd.clear();
    lcd.print("TIMES UP!!!!");
    int x;
    for (x=0; x<1500; x++){
        digitalWrite(3, HIGH);
        delay(5);
        digitalWrite(3, LOW);
        delay(2);
    }    
    lcd.clear();
    lcd.print("Setting Timer");
    timerFormat();
    lcd.blink();
    timerOn = false;
  }

  if (IrReceiver.decode() && currentMillis - lastSignal >= 250){
    /*switch(IrReceiver.decodedIRData.decodedRawData){
      case 3125149440: 
        lcd.clear();
        lcd.print("POWER"); break;
      case 3091726080: 
        lcd.clear();
        lcd.print("FUNC/STOP"); break;
      case 3108437760:
        lcd.clear(); 
        lcd.print("VOL+"); break;
      case 3141861120:
        lcd.clear(); 
        lcd.print("FAST BACK");    break;
      case 3208707840:         
        lcd.clear();
        lcd.print("PAUSE");    break;
      case 3158572800:
        lcd.clear(); 
        lcd.print("FAST FORWARD");   break;
      case 4161273600:
        lcd.clear(); 
        lcd.print("DOWN");    break;
      case 3927310080:
        lcd.clear(); 
        lcd.print("VOL-");    break;
      case 4127850240:
        lcd.clear(); 
        lcd.print("UP");    break;
      case 3860463360:
        lcd.clear(); 
        lcd.print("EQ");    break;
      case 4061003520:
        lcd.clear(); 
        lcd.print("ST/REPT");    break;
      case 3910598400:
        lcd.clear(); 
        lcd.print("0");    break;
      case 4077715200:
        lcd.clear(); 
        lcd.print("1");    break;
      case 3877175040:
        lcd.clear(); 
        lcd.print("2");    break;
      case 2707357440:
        lcd.clear(); 
        lcd.print("3");    break;
      case 4144561920:
        lcd.clear(); 
        lcd.print("4");    break;
      case 3810328320:
        lcd.clear(); 
        lcd.print("5");    break;
      case 2774204160:
        lcd.clear(); 
        lcd.print("6");    break;
      case 3175284480:
        lcd.clear(); 
        lcd.print("7");    break;
      case 2907897600:
        lcd.clear(); 
        lcd.print("8");    break;
      case 3041591040:
        lcd.clear(); 
        lcd.print("9");    break;
    } */
    switch(IrReceiver.decodedIRData.decodedRawData){
      case 3910598400:
        calculateTimer(0);
        setupTimer();
        break;
      case 4077715200:
        calculateTimer(1);
        setupTimer();
        break;
      case 3877175040:
        calculateTimer(2);
        setupTimer();
        break;
      case 2707357440:
        calculateTimer(3);
        setupTimer();
        break;
      case 4144561920:
        calculateTimer(4);
        setupTimer();
        break;
      case 3810328320:
        calculateTimer(5);
        setupTimer();
        break;
      case 2774204160:
        calculateTimer(6);
        setupTimer();
        break;
      case 3175284480:
        calculateTimer(7);
        setupTimer();
        break;
      case 2907897600:
        calculateTimer(8);
        setupTimer();
        break;
      case 3041591040:
        calculateTimer(9);
        setupTimer();
        break;
      case 3158572800: // Fast Forward
        calCursor(1);
        break;
      case 3141861120: // Fast Back
        calCursor(-1);
        break;
      case 3208707840: // pause and play
        if (timer <= 0){
          break;
        }
        timerOn = !(timerOn);
        if (! timerOn){
          lcd.clear();
          lcd.print("Timer paused");
          timerFormat();
        }
        lcd.noBlink();
        break;
      case 3125149440: // Power
        if (timer > 0 && !timerOn){      
        timerOn = true;
        }
        timer = 0;
        break;
    }
    IrReceiver.resume();
    lastSignal = currentMillis;

  }
}
void calculateTimer(int step){
  if (timerOn){
    return;
  }
  int seconds = timer;
  int hours = 0;
  int minutes = 0;
  hours = seconds/ 3600;
  seconds = seconds % 3600;
  minutes = seconds / 60;
  seconds = seconds % 60;
  switch(cursor){
  case 0:
    hours = (hours - (hours % 10)) + step;
    break;      
  case 2:
    minutes = (minutes % 10) + step * 10;
    break;
  case 3:
    minutes = (minutes - (minutes % 10)) + step;
    break;
  case 5:
    seconds =  (seconds % 10) + step * 10;    
    break;
  case 6:
    seconds = (seconds - (seconds % 10)) + step;
    break;
  }
  timer = (hours * 3600) + (minutes * 60) + seconds;
}

void setupTimer(){
  if (timerOn){
    return;
  }
  lcd.clear();
  lcd.print("Setting Timer");
  timerFormat();  
}

void timerFormat(){
  int seconds = timer;
  int hours = 0;
  int minutes = 0;
  hours = seconds / 3600;
  seconds = seconds % 3600;
  minutes = seconds / 60;
  seconds = seconds % 60;
  String total = String(hours);
  total = total + ":";
  if (minutes < 10){
    total =  total + '0';
  }
  total = total + minutes;
  total = total + ":";
  if (seconds < 10){
    total = total + '0';
  }  
  total = total + seconds;
  lcd.setCursor(0,1);
  lcd.print(total);
  lcd.setCursor(cursor, 1);
}

void calCursor(int step){
    if (timerOn){
      return;
    }
    cursor = (cursor + step) % 7;
    if (cursor == 1 || cursor == 4){
      cursor += step;
    }
    else if (cursor < 0){
      cursor = 6;
    }
    lcd.setCursor(cursor, 1);
    lcd.blink();
}
